const { Builder, Browser, By, Key, until, Select } = require('selenium-webdriver')
const config = require('../config.json');

(async () => {
  const driver = await new Builder().forBrowser(Browser.FIREFOX).build()
  try {
    await driver.manage().setTimeouts({ implicit: 2000 })
    await driver.get(config.libcalUrl)

    // Pick the proper date
    await driver.findElement(By.className('fc-goToDate-button')).click()
    await driver.wait(until.elementIsVisible(driver.findElement(By.className('datepicker-days'))))
    await driver.actions().sendKeys(Key.ARROW_DOWN).sendKeys(Key.RETURN).perform()

    // Pick the correct date cell
    const bookingCell = await driver.findElement(By.xpath(`//*[starts-with(@title, '${config.bookingTime}') and contains(@title,'- ${config.roomName} -')]`))
    await bookingCell.click()

    // Pick the longest available booking time
    const dateSelectElement = await driver.findElement(By.className('b-end-date'))
    const dateSelect = new Select(dateSelectElement)
    await dateSelectElement.click()
    const options = await dateSelect.getOptions()
    await options[options.length - 1].click()

    // Submit time
    const submitButton = await driver.findElement(By.id('submit_times'))
    await submitButton.click()

    // Continue
    const termsButton = await driver.findElement(By.id('terms_accept'))
    await termsButton.click()

    // Fill in name
    await driver.wait(until.elementIsVisible(driver.findElement(By.id('fname'))))
    await driver.findElement(By.id('fname')).sendKeys(config.firstName)
    await driver.findElement(By.id('lname')).sendKeys(config.lastName)
    await driver.findElement(By.id('email')).sendKeys(config.email)

    // Submit booking
    await driver.findElement(By.id('btn-form-submit')).click()

    // Pause
    await driver.sleep(20000)
  } finally {
    await driver.quit()
  }
})()
